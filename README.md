# MC-URL
监听当前页面是否可见状态，可用于客户端转入后台又转回后触发事件


## 索引
* [示例](#示例)
* [版本](#版本)

## 开始
NPM: 
```npm
npm i mc-url
```

## 示例

```javascript
import vis from 'mc-vis';

vis({
  enter(){
    console.log('当前页面激活状态')
  },
  leave(){
    console.log('当前页面转入后台')
  },
  then(){
    console.log('显示或隐藏都触发')
  } 
})
```

## 版本
```text
v1.0.0
1. 核心功能已完成
```
