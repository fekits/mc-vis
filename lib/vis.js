/*
    * 判断当前窗口是否在活动状态？当活动状态发生变化时触发一个回调事件。
    * env.status({
    *  then:function(){状态发生变化是触发},
    *  enter:function(){窗口变化为激活时触发},
    *  leave:function(){窗口变化为非激活状态时触发}
    * })
    *
    * */
!(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory();
  } else {
    root.vis = factory();
  }
}(this, function () {
  return function (param) {
    if (param) {
      var hidden = 'hidden',
        webkitHidden = 'webkitHidden',
        mozHidden = 'mozHidden',
        msHidden = 'msHidden';
      var hiddenProperty = hidden in document ? hidden : webkitHidden in document ? webkitHidden : mozHidden in document ? mozHidden : msHidden in document ? msHidden : null;
      var visibilityChangeEvent = hiddenProperty.replace(/hidden/i, 'visibilitychange');
      var vis = function () {
        param.then && param.then(!document[hiddenProperty]);
        if (!document[hiddenProperty]) {
          param.enter && param.enter(true);
        } else {
          param.leave && param.leave(false);
        }
      };
      document.addEventListener(visibilityChangeEvent, vis);
    }
  };
}));
