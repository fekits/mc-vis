import '../css/main.scss';

import './route';

// 代码着色插件
import McTinting from 'mc-tinting';
// 新建一个代码着色实例
new McTinting();

// 实例代码
import vis from '../../lib/vis';

vis({
  enter () {
    console.log('enter');
  },
  leave () {
    console.log('leave');
  }
});
